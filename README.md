# How to start

First run the ./install.sh script, you will need Istio and Tiller properly configured in your cluster.

Then, log in in the admin console with the provided credentials in the Chart template.

Note you have already a json in this project with the realm configuration, just follow this:

[https://redhat-developer-demos.github.io/istio-tutorial/istio-tutorial/1.0.x/8jwt.html](https://redhat-developer-demos.github.io/istio-tutorial/istio-tutorial/1.0.x/8jwt.html)
